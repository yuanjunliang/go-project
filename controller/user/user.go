package UserController

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	mydb "github.com/goweb/db"
	module "github.com/goweb/module/mysql"
)

// 增
func UserRegist(c *gin.Context) {
	var user module.UserModule

	if err := c.BindJSON(&user); err != nil {
		log.Fatalln(err)
	}

	fmt.Println(user.UserName)
	fmt.Println(user.Pwd)
	username := user.UserName
	password := user.Pwd
	db := mydb.MysqlDB()
	sql := "INSERT INTO test(username,pwd) VALUES(?,?)"
	res, err := db.Exec(sql, username, password)
	if err != nil {
		log.Fatalln(err)
	}

	c.JSON(http.StatusOK, gin.H{
		"response": res,
	})
}

// 删
func GetUserInfo(c *gin.Context) {
	db := mydb.MysqlDB()
	sql := "SELECT * FROM test"
	res, err := db.Query(sql)
	if err != nil {
		log.Fatalln(err)
	}

	defer res.Close()
	users := make([]module.UserModule, 0)

	for res.Next() {
		var user module.UserModule
		res.Scan(&user.UserName, &user.Pwd)
		users = append(users, user)
	}
	c.JSON(http.StatusOK, gin.H{
		"response": users,
	})
}

// 改
func UpdateUserInfo(c *gin.Context) {
	var user module.UserModule
	if err := c.BindJSON(&user); err != nil {
		log.Println(err)
	}
	username := user.UserName
	fmt.Println("username", username)
	db := mydb.MysqlDB()
	sql := "UPDATE test SET username=?"
	res, err := db.Exec(sql, "junliangyuankkk")
	if err != nil {
		log.Fatalln(err)
	}
	c.JSON(http.StatusOK, gin.H{
		"response": res,
	})
}

// 查
func DeleteUser(c *gin.Context) {
	var user module.UserModule
	if err := c.BindJSON(&user); err != nil {
		log.Println(err)
	}
	username := user.UserName
	db := mydb.MysqlDB()
	sql := "DELETE FROM test WHERE username=?"
	res, err := db.Exec(sql, username)
	if err != nil {
		log.Println(err)
	}
	c.JSON(http.StatusOK, gin.H{
		"response": res,
	})
}
