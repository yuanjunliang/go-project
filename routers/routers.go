package routers

import (
	"github.com/gin-gonic/gin"
	"github.com/goweb/controller/mongo"
	"github.com/goweb/controller/user"
)

func Routes(router *gin.Engine) {
	// Router Group
	v1 := router.Group("/v1")

	// UserController
	rUser := v1.Group("/user")
	rUser.POST("/user_regist", UserController.UserRegist)
	rUser.GET("/get_userinfo", UserController.GetUserInfo)
	rUser.POST("/update_user_info", UserController.UpdateUserInfo)
	rUser.POST("/delete_user", UserController.DeleteUser)

	// MongoController
	rMongo := v1.Group("/mongo")
	rMongo.POST("/insert_user", MongoController.Insert)
	rMongo.POST("/find_user", MongoController.Find)
	rMongo.POST("/update_user", MongoController.Update)
	rMongo.POST("/delete_user", MongoController.Delete)
}
