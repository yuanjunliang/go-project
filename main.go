package main

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/goweb/db"
	"github.com/goweb/routers"
)

func main() {
	router := gin.Default()
	s := &http.Server{
		Addr:           ":8080",
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	db.InitMysqlDB()
	db.InitMongoDB("test")
	routers.Routes(router)
	s.ListenAndServe()
}
