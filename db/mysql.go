package db

import (
	"database/sql"
	"log"
)

var mydb *sql.DB

func MysqlDB() *sql.DB {
	return mydb
}

func InitMysqlDB() {
	db, err := sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/blockchainclub")
	if err != nil {
		println("mysql connected failed")
		log.Fatal(err)
	}
	print("mysql connected success")
	// defer db.Close()

	// 设置最大连接数
	db.SetMaxIdleConns(20)
	db.SetMaxOpenConns(20)

	if err := db.Ping(); err != nil {
		log.Fatalln(err)
	}
	mydb = db
}
