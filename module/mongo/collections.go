package module

import (
	db "github.com/goweb/db"
	mgo "gopkg.in/mgo.v2"
)

func UserCollection() *mgo.Collection {
	mongo := db.GetMongoDB()
	return mongo.C("users")
}
