package module

// users
type UserModule struct {
	UserName string `json:"username" form:"username"`
	Pwd      string `json:"pwd" form:"pwd"`
}
